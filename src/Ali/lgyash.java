package Ali;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Parameters;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.AfterClass;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;


public class lgyash {
		WebDriver driver;
		
		@BeforeClass
		public void BeforeClass() {
			System.out.println("ali");
		}
		@AfterClass
		public void AfterClass() {
			System.out.println("mohamed");
		}
		
		@BeforeTest
		public void Ali() {
			System.out.println("ali");
		}
		@AfterTest
		public void Mohamed() {
			System.out.println("sameer");
		}
		@Parameters({"URL"})
		@BeforeMethod(enabled = false)
		public void setupMethod() {
			System.setProperty("webdriver.chrome.driver","E:\\Study\\chromedriver.exe");
			driver =new ChromeDriver();
			driver.get("www.google.com");
		}
		
		@AfterMethod(enabled = false)
		public void endCase() {
			driver.quit();
		}

		
	
		@Test (priority=2, enabled = false)
		public void testTitle() {
		SoftAssert soso = new SoftAssert();
		String ExpectedResult = "Free CRM software for any Business";
		String ActualResult = driver.getTitle();
		System.out.println(ActualResult);
		soso.assertEquals(ExpectedResult, ActualResult);
		System.out.println("soso");
		soso.assertAll();
		}
//		
		@Test (priority=2, enabled = false)
		public void urlCheck () {
			String ExpectedResult = "https://freecrm.com/";
			String ActualResult = driver.getCurrentUrl();
			System.out.println(ActualResult);
			Assert.assertEquals(ExpectedResult, ActualResult);
		}
//		
		@Test (priority=3 , enabled = false)
		public void logoDisplayed() {
			boolean ExpectedResult = true;
			List<WebElement> Actual = driver.findElements(By.xpath("//div[@class='rd-navbar-brand']"));
			WebElement Actual1 = Actual.get(0);
			boolean ActuaLResult =Actual1.isDisplayed();
			System.out.println(ActuaLResult);
			Assert.assertEquals(ExpectedResult, ActuaLResult);
		}
		
		@Test (priority=4, enabled = false)
		public void loginTest() {
			String ExpectedResult = "Logged in";
			driver.manage().timeouts().implicitlyWait(10,TimeUnit.SECONDS);
			WebElement Login = driver.findElement(By.xpath("//a[@href='https://ui.freecrm.com']"));
			Login.click();
			
			WebElement loginPageEmail = driver.findElement(By.xpath("//input[@placeholder='E-mail address']"));
			loginPageEmail.sendKeys("ali.abdullah.abdelghany@gmail.com");
			WebElement loginPagePass = driver.findElement(By.xpath("//input[@placeholder='Password']"));
			loginPagePass.sendKeys("Aliali123321");	
			WebElement loginClick = driver.findElement(By.cssSelector("div[class='ui fluid large blue submit button']"));
			loginClick.click();
			boolean ActualResult1 = loginClick.isSelected();
			if(ActualResult1=true) {
				String ActualResult = "Logged in";
				Assert.assertEquals(ExpectedResult, ActualResult);
			}
			
		}
		
		
		
}
