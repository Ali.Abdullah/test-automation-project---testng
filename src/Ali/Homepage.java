package Ali;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

public class Homepage {
	WebDriver driver;

	@Test (priority=1,dataProvider="SearchProvider")
	public void LoginPage(String UserName, String Password) {
//		String ExpectedResult = "Logged in";
		WebElement Login = driver.findElement(By.xpath("//a[@href='https://ui.freecrm.com']"));
		Login.click();
		WebElement loginPageEmail = driver.findElement(By.xpath("//input[@placeholder='E-mail address']"));
		loginPageEmail.sendKeys(UserName);
		WebElement loginPagePass = driver.findElement(By.xpath("//input[@placeholder='Password']"));
		loginPagePass.sendKeys(Password);	
		WebElement loginClick = driver.findElement(By.cssSelector("div[class='ui fluid large blue submit button']"));
		loginClick.click();
	}
	
	@Parameters({"URL"})
	@BeforeMethod 
	public void PreCondition(String URL) {
		System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir")+"/Resources/chromedriver.exe");	
//		System.setProperty("Webdriver.Chrome.driver","Resources/chromedriver.exe");
		driver = new ChromeDriver();
		driver.get(URL);
		driver.manage().window().maximize();
//		driver.manage().timeouts().implicitlyWait(10,TimeUnit.SECONDS);
	}
	
	@AfterMethod
	public void PostCondition(){
		driver.quit();
	}
	
	
	
//	@Test (priority=1)
//	public void logoDisplayed() {
//		boolean ExpectedResult = true;
//		List<WebElement> Actual = driver.findElements(By.xpath("//div[@class='rd-navbar-brand']"));
//		WebElement Actual1 = Actual.get(0);
//		boolean ActuaLResult =Actual1.isDisplayed();
//		System.out.println(ActuaLResult);
//		Assert.assertEquals(ExpectedResult, ActuaLResult);
//	}
	
	
	@DataProvider(name="SearchProvider")
	public Object[][] MyData() {
		
		Object[][] ali = new Object[4][2];
		ali[0][0] = "Ali";
		ali[0][1] = "ahmed";
		ali[1][0] = "mohamwed";
		ali[1][1] = "kareem";
		ali[2][0] = "samir";
		ali[2][1] = "samira";
		ali[3][0] = "hala";
		ali[3][1] = "angham";
		return ali;
		
	}

	


	}
